extends Area2D

const ATTRACTION_SPEED : float = 250
@export var value : int = 1

var attract_target : Node2D = null

func attracts_to(target : Node2D):
	attract_target = target
	
func _physics_process(delta):
	if attract_target != null:
		var direction = global_position.direction_to(attract_target.global_position)
		global_position += direction * ATTRACTION_SPEED * delta


func _on_body_entered(body):
	if body.has_method("collect"):
		body.collect(value)
		queue_free()
