extends Area2D

class_name Weapon

const BULLET = preload("res://Scenes/Objects/bullet.tscn")

@export var attack_period : float = 0.1;
@export var min_attack_period : float = 0.1;

var cooldown : float = 0;

var areas : Dictionary = {}

func _init():
	PhysicsServer2D.area_set_area_monitor_callback(get_rid(), _area_monitor)

func _area_monitor(status : int, rid : RID, _instance_id : int, _area_shape_idx : int, _self_shape_idx : int):
	if status == PhysicsServer2D.AREA_BODY_ADDED:
		areas[rid] = null
	elif status == PhysicsServer2D.AREA_BODY_REMOVED:
		areas.erase(rid)

func get_position_of_closest_area() -> Variant:
	var closest_position : Variant = null
	var closest_target_distance : float = INF
	for a in areas.keys() :
		var p = Enemy.get_enemy_position(a)
		if p:
			var distance = p.distance_to(global_position)
			if(distance < closest_target_distance):
				closest_target_distance = distance
				closest_position = p
	return closest_position

func _physics_process(delta):
	if(cooldown <= 0):
		cooldown = 0
	cooldown -= delta
	if(cooldown <= 0):
		var target = get_position_of_closest_area()
		if(target):
			fire_at(target)

func fire_at(target : Vector2):
	var instance = BULLET.instantiate()
	instance.global_position = global_position
	instance.global_rotation = Vector2.UP.angle_to(target - global_position)
	$"..".get_parent().add_child(instance)
	if attack_period < min_attack_period:
		attack_period = min_attack_period
	cooldown += attack_period
		
func find_closest_target() -> Node2D:
	var closest_target : Node2D = null
	var closest_target_distance : float = INF
	var targets = get_overlapping_areas()
	for target in targets :
		var distance = target.global_position.distance_to(global_position)
		if(distance < closest_target_distance):
			closest_target_distance = distance
			closest_target = target
	return closest_target
