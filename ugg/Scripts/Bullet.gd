extends ShapeCast2D

@export var speed : float = 500.0
@export var damage : float = 1
@export var max_range : float = 1000.0

var distance_traveled = 0

func _physics_process(delta):
	var direction = Vector2.UP.rotated(global_rotation)
	global_position += direction * speed * delta
	distance_traveled += speed * delta
	if distance_traveled > max_range:
		queue_free()
	if is_colliding() and get_collision_count() >= 1:
		Enemy.hit_enemy(get_collider_rid(0), damage)
		queue_free()
