@tool
@icon("res://Assets/NodeIcons/Camera2D.svg")
extends Node2D
class_name CicleCamera2D

@export_range(0, 1000) var view_radius : float = 600.0

func _process(_delta):
	if not Engine.is_editor_hint():
		var viewport = get_viewport()
		var view_scale : float = (viewport.size.length()/view_radius)/2
		viewport.canvas_transform = global_transform.affine_inverse().scaled(Vector2(view_scale, view_scale))
		viewport.canvas_transform.origin += Vector2(viewport.size)/2.0
	else:
		queue_redraw()

func _draw():
	if Engine.is_editor_hint():
		draw_arc(Vector2(0,0), view_radius, 0, TAU, 33, Color(255,0,0))
