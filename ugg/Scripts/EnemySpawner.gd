extends Node2D

class_name EnemySpawner

@export var enemy_wave : EnemyWave

@onready var camera : CicleCamera2D = %CircleCamera2D
var cooldown : float = 0

func _ready():
	Enemy.root_node = get_parent().get_parent()
	Enemy.player_position = global_position
	Enemy.space = get_world_2d().space
	
	Performance.add_custom_monitor("Game/Enemies", Enemy.get_enemy_count)
	for i in Enemy.enemy_pool_size:
		Enemy.enemy_pool.append(Enemy.new())

func _process(delta):
	Enemy.player_position = global_position
	Enemy.delta = delta
	for i in Enemy.enemy_pool_size:
		Enemy.enemy_pool[i].process()
	for i in Enemy.enemy_pool_size:
		Enemy.enemy_pool[i].update_position()
	cooldown -= delta
	while cooldown <= 0:
		spawn_enemy()
		cooldown += enemy_wave.spawn_period

func get_random_spawn_point() -> Vector2:
	var random_direction : Vector2 = Vector2.from_angle(randf_range(0, TAU))
	var view_margin : float = 10
	return camera.global_position + random_direction * (camera.view_radius + view_margin)
	
func spawn_enemy():
	Enemy.spawn(enemy_wave.get_random_enemy(), get_random_spawn_point())
