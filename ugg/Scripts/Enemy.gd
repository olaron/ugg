class_name Enemy

const DROP = preload("res://Scenes/Objects/drop.tscn")

const max_speed : float = 100.0
const max_acceleration : float = 10000.0
const acceleration_speed : float = 10000.0

static var enemy_pool : Array[Enemy] = []
static var enemy_pool_size : int = 2000 
static var space : RID
static var root_node : Node2D
static var player_position : Vector2
static var enemies_count : int = 0
static var delta : float
static var areamap : Dictionary = {}
var health : float = 1.0
var active : bool = false
var transform : Transform2D = Transform2D()
var cid : RID = RenderingServer.canvas_item_create()
var pid : RID = PhysicsServer2D.area_create()
var forces : Vector2 = Vector2.ZERO
var velocity : Vector2 = Vector2.ZERO
var colliding_object_ids : Dictionary = {}
var position : Vector2 = Vector2.ZERO
var ressource : EnemyResource

static func drop_at(p : Vector2):
	var d = DROP.instantiate()
	d.global_position = p
	root_node.add_child(d)

func _spawn(enemy : EnemyResource, p : Vector2, i : int):
	ressource = enemy
	health = enemy.health
	enemies_count += 1
	active = true
	transform.origin = p
	position = p
	RenderingServer.canvas_item_set_parent(cid, root_node.get_canvas_item())
	var sprite_rect = Rect2(-ressource.texture.get_width()/2.0, -ressource.texture.get_height(), ressource.texture.get_width(), ressource.texture.get_height())
	RenderingServer.canvas_item_add_texture_rect(cid, sprite_rect, ressource.texture.get_rid())
	RenderingServer.canvas_item_set_transform(cid, transform)
	RenderingServer.canvas_item_set_default_texture_filter(cid, RenderingServer.CANVAS_ITEM_TEXTURE_FILTER_NEAREST)
	
	areamap[pid] = i
	PhysicsServer2D.area_attach_object_instance_id(pid, get_instance_id())
	PhysicsServer2D.area_set_area_monitor_callback(pid, _area_monitor)
	PhysicsServer2D.area_set_monitor_callback(pid, _body_monitor)
	PhysicsServer2D.area_set_monitorable(pid,true)
	PhysicsServer2D.area_add_shape(pid, ressource.shape)
	PhysicsServer2D.area_set_space(pid, space)
	PhysicsServer2D.area_set_collision_layer(pid,4)
	PhysicsServer2D.area_set_collision_mask(pid,6)
	PhysicsServer2D.area_set_transform(pid, transform)

static func get_free_index():
	for i in enemy_pool_size:
		if !enemy_pool[i].active:
			return i
	return -1

static func spawn(enemy : EnemyResource, p : Vector2):
	var i = get_free_index()
	if i >= 0:
		enemy_pool[i]._spawn(enemy, p, i)
	
func _body_monitor(status : int, _rid : RID, instance_id : int, _shape_idx : int, _self_shape_idx : int):
	if status == PhysicsServer2D.AREA_BODY_ADDED:
		colliding_object_ids[instance_id] = null
	elif status == PhysicsServer2D.AREA_BODY_REMOVED:
		colliding_object_ids.erase(instance_id)
	
func _area_monitor(status : int, _rid : RID, instance_id : int, _shape_idx : int, _self_shape_idx : int):
	if status == PhysicsServer2D.AREA_BODY_ADDED:
		colliding_object_ids[instance_id] = null
	elif status == PhysicsServer2D.AREA_BODY_REMOVED:
		colliding_object_ids.erase(instance_id)
	
func hit(damage: float):
	if !active: return
	if health <= 0: return
	health -= damage
	if health <= 0:
		Enemy.drop_at(position)
		call_deferred("delete")
	
func delete():
	if !active: return
	enemies_count -= 1
	active = false
	RenderingServer.canvas_item_clear(cid)
	PhysicsServer2D.area_clear_shapes(pid)
	areamap.erase(pid)
	colliding_object_ids.clear()

func compute_penetration(p : Vector2, r : float) -> Vector2:
	var d = p - position
	if d.length() > r + ressource.radius:
		return Vector2.ZERO
	if d.length() == 0:
		return Vector2.from_angle(TAU * randf())
	return d.normalized() * (r + ressource.radius - d.length())

func compute_forces():
	forces = Vector2.ZERO
	for id in colliding_object_ids.keys():
		var obj = instance_from_id(id)
		if obj.has_method("compute_penetration"):
			var f : Vector2 = obj.compute_penetration(position, ressource.radius)
			forces += f * (f.normalized().dot(-velocity.normalized()) + 1) * 2
		else:
			prints("Object id", obj, "collide with enemy but does not implement compute_penetration")


func process():
	if !active: return
	var direction = position.direction_to(player_position)
	velocity = direction * max_speed
	velocity = velocity.limit_length(max_speed)
	compute_forces()
	var move = velocity * delta + forces
	position += move.limit_length(max_speed * delta * ressource.speed)

func update_position():
	if !active: return
	var t = transform
	t.origin = position
	RenderingServer.canvas_item_set_transform(cid, t)
	if position.distance_to(transform.origin) > 10:
		transform.origin = position
		PhysicsServer2D.call_deferred("area_set_transform",pid, transform)
		if transform.origin.distance_to(player_position) > 1000:
			delete()

static func get_enemy_count():
	return enemies_count

static func hit_enemy(rid : RID, damage : float):
	if Enemy.areamap.has(rid):
		Enemy.enemy_pool[Enemy.areamap[rid]].hit(damage)

static func get_enemy_position(rid : RID) -> Variant:
	if Enemy.areamap.has(rid):
		return Enemy.enemy_pool[Enemy.areamap[rid]].position
	return null
