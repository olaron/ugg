extends ShapeCast2D

class_name GenericBullet

@export var resource : BulletResource

var distance_traveled = 0

@onready var sprite : Sprite2D = $Sprite2D

func _ready():
	sprite.texture = resource.texture

func _physics_process(delta):
	var direction = Vector2.UP.rotated(global_rotation)
	global_position += direction * resource.speed * delta
	distance_traveled += resource.speed * delta
	if distance_traveled > resource.max_range:
		queue_free()
	if is_colliding() and get_collision_count() >= 1:
		Enemy.hit_enemy(get_collider_rid(0), resource.damage)
		if resource.pierce > 0:
			add_exception_rid(get_collider_rid(0))
			resource.pierce -= 1
		else:
			queue_free()
