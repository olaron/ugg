extends Area2D

func _on_area_entered(area):
	if area.has_method("attracts_to"):
		area.attracts_to(self)
