extends Area2D

class_name GenericWeapon

const BULLET = preload("res://Scenes/Objects/GenericBullet.tscn")

@export var resource : WeaponResource

@onready var shape : CircleShape2D = $"CollisionShape2D".shape

var cooldown : float = 0;

var enemy_ids : Dictionary = {}

func _ready():
	shape.radius = resource.range
	PhysicsServer2D.area_set_area_monitor_callback(get_rid(), _area_monitor)

func _area_monitor(status : int, _rid : RID, instance_id : int, _area_shape_idx : int, _self_shape_idx : int):
	if status == PhysicsServer2D.AREA_BODY_ADDED:
		enemy_ids[instance_id] = null
	elif status == PhysicsServer2D.AREA_BODY_REMOVED:
		enemy_ids.erase(instance_id)

func get_position_of_closest_area() -> Variant:
	var closest_position : Variant = null
	var closest_target_distance : float = INF
	for enemy_id in enemy_ids.keys() :
		var e : Enemy = instance_from_id(enemy_id)
		if e:
			var p = e.position
			var distance = p.distance_to(global_position)
			if(distance < closest_target_distance):
				closest_target_distance = distance
				closest_position = p
	return closest_position

func _physics_process(delta):
	if(cooldown <= 0):
		cooldown = 0
	cooldown -= delta
	if(cooldown <= 0):
		var target = get_position_of_closest_area()
		if(target):
			fire_at(target)

func fire_at(target : Vector2):
	var instance : GenericBullet = BULLET.instantiate()
	instance.resource = resource.bullet.duplicate()
	instance.global_position = global_position
	instance.global_rotation = Vector2.UP.angle_to(target - global_position)
	$"..".get_parent().add_child(instance)
	cooldown += resource.fire_period
		
func find_closest_target() -> Node2D:
	var closest_target : Node2D = null
	var closest_target_distance : float = INF
	var targets = get_overlapping_areas()
	for target in targets :
		var distance = target.global_position.distance_to(global_position)
		if(distance < closest_target_distance):
			closest_target_distance = distance
			closest_target = target
	return closest_target
