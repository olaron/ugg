extends CharacterBody2D

@export var walk_speed : float = 200.0

func _process(delta):
	var input_dir = Input.get_vector("left", "right", "up", "down")
	global_position += input_dir * walk_speed * delta

func collect(_value):
	pass

func compute_penetration(p : Vector2, r : float) -> Vector2:
	var d = p - position
	var cs : CollisionShape2D = $CollisionShape2D
	var radius = cs.get_shape().radius
	if d.length() > r + radius:
		return Vector2.ZERO
	if d.length() == 0:
		return Vector2.from_angle(TAU * randf())
	return d.normalized() * (r + radius - d.length())
