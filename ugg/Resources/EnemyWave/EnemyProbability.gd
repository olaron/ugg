extends Resource

class_name EnemyProbability

@export var enemy : EnemyResource
@export var probability : float
