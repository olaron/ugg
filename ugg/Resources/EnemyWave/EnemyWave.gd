extends Resource

class_name EnemyWave

var probability_sum : float

@export var spawn_period : float

@export var enemies : Array[EnemyProbability]:
	set(new_enemies):
		enemies = new_enemies
		probability_sum = 0
		for e in enemies:
			probability_sum += e.probability
		emit_changed()

func get_random_enemy() -> EnemyResource:
	var r = randf() * probability_sum
	for e in enemies:
		r -= e.probability
		if r <= 0:
			return e.enemy
	push_error("should be unreachable")
	return enemies.back().enemy
