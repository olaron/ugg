extends Resource

class_name WeaponResource

@export var bullet : BulletResource
@warning_ignore("shadowed_global_identifier")
@export var range : float
@export var fire_period : float
