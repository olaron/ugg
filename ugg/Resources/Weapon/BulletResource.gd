extends Resource

class_name BulletResource

@export var texture : Texture2D
@export var hitbox_size : float
@export var damage : float
@export var speed : float
@export var max_range : float

@export var pierce : int
@export var bounce : int
@export var homing : bool
