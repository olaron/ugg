extends Resource

class_name EnemyResource

@export var texture : Texture2D = preload("res://Resources/Enemies/Enemy1.png")
@export var health : float = 1
@export var speed : float = 1

var radius : float
var shape : RID

func _init():
	radius = texture.get_width()/2.0
	shape = PhysicsServer2D.circle_shape_create()
	PhysicsServer2D.shape_set_data(shape, radius)
